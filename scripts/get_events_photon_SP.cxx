#include "TFile.h"
#include "TH1F.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h" 

#include <iostream>
#include <fstream>  

TString files_dir = "/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/fullRun2/FinalNtuples/";

void loop(TString input_name, TString output_name)
{
  std::cout << "processing " << input_name << std::endl;

  bool is_data = input_name.BeginsWith("data");
  int previous_run_number = 0;
  
  TFile *myFile = TFile::Open(files_dir+input_name);
  
  TTreeReader reader("SinglePhoton", myFile);
  
  TTreeReaderValue<Int_t> HLT_g10_loose(reader, "HLT_g10_loose");
  TTreeReaderValue<Int_t> HLT_g15_loose_L1EM7(reader, "HLT_g15_loose_L1EM7");
  TTreeReaderValue<Int_t> HLT_g20_loose_L1EM12(reader, "HLT_g20_loose_L1EM12");
  TTreeReaderValue<Int_t> HLT_g25_loose_L1EM15(reader, "HLT_g25_loose_L1EM15");
  TTreeReaderValue<Int_t> HLT_g35_loose_L1EM15(reader, "HLT_g35_loose_L1EM15");
  TTreeReaderValue<Int_t> HLT_g40_loose_L1EM15(reader, "HLT_g40_loose_L1EM15");
  TTreeReaderValue<Int_t> HLT_g45_loose_L1EM15(reader, "HLT_g45_loose_L1EM15");
  TTreeReaderValue<Int_t> HLT_g50_loose_L1EM15(reader, "HLT_g50_loose_L1EM15");
  TTreeReaderValue<Int_t> HLT_g60_loose(reader, "HLT_g60_loose");
  TTreeReaderValue<Int_t> HLT_g70_loose(reader, "HLT_g70_loose");
  TTreeReaderValue<Int_t> HLT_g80_loose(reader, "HLT_g80_loose");
  TTreeReaderValue<Int_t> HLT_g100_loose(reader, "HLT_g100_loose");
  TTreeReaderValue<Int_t> HLT_g120_loose(reader, "HLT_g120_loose");
  TTreeReaderValue<Int_t> HLT_g140_loose(reader, "HLT_g140_loose");
  
  TTreeReaderValue<Float_t> y_pt(reader, "y_pt");
  TTreeReaderValue<Float_t> y_pt_cl(reader, "y_pt_cl");
  TTreeReaderValue<Float_t> y_eta_cl_s2(reader, "y_eta_cl_s2");
  TTreeReaderValue<Float_t> y_phi_cl(reader, "y_phi_cl");
  TTreeReaderValue<Bool_t> y_IsLoose(reader, "y_IsLoose");
  TTreeReaderValue<Bool_t> y_iso_FixedCutLoose(reader, "y_iso_FixedCutLoose");

  TTreeReaderValue<Float_t> evt_runNo(reader, "evt_runNo");
  TTreeReaderValue<ULong64_t> evt_eventNo(reader, "evt_eventNo");

  TTreeReaderValue<Float_t> mcid(reader, "mcid");
  
  // Loop over all entries of the TTree or TChain.
  Long64_t entry = 0;
  Long64_t entries = reader.GetEntries(true);
  
  std::ofstream outfile;  
    
  if (is_data)
    outfile.open(Form("%s.txt", output_name.Data()), std::ofstream::out);
  
  while (reader.Next()) {
    
    if (entry % 1000000 == 0)
      std::cout << "Processing " <<  entry << "/" << entries << "..." << std::endl;
    
    entry ++;
    
    // trigger
    if (is_data) {
      if (*HLT_g10_loose == 0 && *HLT_g15_loose_L1EM7 == 0 && *HLT_g20_loose_L1EM12 == 0 && *HLT_g25_loose_L1EM15 == 0 && *HLT_g35_loose_L1EM15 == 0 && *HLT_g40_loose_L1EM15 == 0 && *HLT_g45_loose_L1EM15 == 0 && *HLT_g50_loose_L1EM15 == 0 && *HLT_g60_loose == 0 && *HLT_g70_loose == 0 && *HLT_g80_loose == 0 && *HLT_g100_loose == 0 && *HLT_g120_loose == 0 && *HLT_g140_loose == 0)
        continue;
    }

    // acceptance selection 
    if (fabs(*y_eta_cl_s2) > 2.37 || (fabs(*y_eta_cl_s2) > 1.37 && fabs(*y_eta_cl_s2) < 1.52))
      continue;
    
    if (*y_pt < 25)
      continue;
    
    if (*y_IsLoose == 0)
      continue;
    
    if (*y_iso_FixedCutLoose == 0)
      continue;

    if (is_data) {
      outfile << *evt_runNo << " " << *evt_eventNo << " " << *y_eta_cl_s2 << " " << *y_phi_cl << std::endl;
    }
    else {
      
      if (*mcid != previous_run_number) {
        if (outfile.is_open())
          outfile.close();
        
        outfile.open(Form("%s_%i.txt", output_name.Data(), int(*mcid)), std::ofstream::out);
        previous_run_number = *mcid;
      }
      outfile << *mcid << " " << *evt_eventNo << " " << *y_eta_cl_s2 << " " << *y_phi_cl << std::endl;
    }
      
  }
  
  outfile.close();
};

void get_events_photon_SP() 
{
  loop("data15_276262_284484_GRL_p3930_Rel21_AB21.2.94_v0.root", "events_SP_data15");
  loop("data16_297730_311481_GRL_p3930_Rel21_AB21.2.94_v0.root", "events_SP_data16");
  loop("data17_325713_340453_GRL_p3930_Rel21_AB21.2.94_v0.root", "events_SP_data17");
  loop("data18_348885_364292_GRL_p3930_Rel21_AB21.2.94_v0.root", "events_SP_data18");

  loop("PyPt17_inf_mc16a_p3931_Rel21_AB21.2.94_v0.root",    "events_SP_gammajet_mc16a");
  loop("PyPt17_inf_mc16d_p3931_Rel21_AB21.2.94_v0.root",    "events_SP_gammajet_mc16d");
  loop("PyPt17_inf_mc16e_p3931_Rel21_AB21.2.94_v0.root",    "events_SP_gammajet_mc16e");
  
  loop("Py8_jetjet_mc16a_p3929_Rel21_AB21.2.94_v0.root",    "events_SP_jetjet_mc16a");
  loop("Py8_jetjet_mc16d_p3929_Rel21_AB21.2.94_v0.root",    "events_SP_jetjet_mc16d");
  loop("Py8_jetjet_mc16e_p3929_Rel21_AB21.2.94_v0.root",    "events_SP_jetjet_mc16e");
};
