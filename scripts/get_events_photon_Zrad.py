import ROOT

files_loc_ZLLG = '/eos/atlas/atlascerngroupdisk/perf-egamma/photonID/NTUP_ZLLG/'


dsids = {
    'Sh_224_NN30NNLO_eegamma_LO_pty_7_15':      '366140',
    'Sh_224_NN30NNLO_eegamma_LO_pty_15_35':     '366141',
    'Sh_224_NN30NNLO_eegamma_LO_pty_35_70':     '366142',
    'Sh_224_NN30NNLO_eegamma_LO_pty_70_140':    '366143',
    'Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS': '366144',

    'Sh_224_NN30NNLO_mumugamma_LO_pty_7_15':      '366145',
    'Sh_224_NN30NNLO_mumugamma_LO_pty_15_35':     '366146',
    'Sh_224_NN30NNLO_mumugamma_LO_pty_35_70':     '366147',
    'Sh_224_NN30NNLO_mumugamma_LO_pty_70_140':    '366148',
    'Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS': '366149',
}

files = [
    'data15_13TeV/00-03-01/target_data15_Zeeg_p3948.root',
    'data16_13TeV/00-03-01/target_data16_Zeeg_p3948.root',
    'data15_13TeV/00-03-01/target_data15_Zmumug_p3948.root',
    'data16_13TeV/00-03-01/target_data16_Zmumug_p3948.root',
    'data17_13TeV/00-03-01/target_data17_Zeeg_p3948.root',
    'data17_13TeV/00-03-01/target_data17_Zmumug_p3948.root',
    'data18_13TeV/00-03-01/target_data18_Zeeg_p3948.root',
    'data18_13TeV/00-03-01/target_data18_Zmumug_p3948.root',

    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.DAOD_EGAM3.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.DAOD_EGAM3.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.DAOD_EGAM3.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.DAOD_EGAM3.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.DAOD_EGAM3.e7006_e5984_s3126_r9364_r9315_p3956.root',
    
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.DAOD_EGAM4.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.DAOD_EGAM4.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.DAOD_EGAM4.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.DAOD_EGAM4.e7006_e5984_s3126_r9364_r9315_p3956.root',
    'mc16a_13TeV/00-03-01/mc16a.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.DAOD_EGAM4.e7006_e5984_s3126_r9364_r9315_p3956.root',

    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956.root',
    
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.DAOD_EGAM4.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.DAOD_EGAM4.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.DAOD_EGAM4.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.DAOD_EGAM4.e7006_e5984_s3126_r10201_r10210_p3956.root',
    'mc16d_13TeV/00-03-01/mc16d.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.DAOD_EGAM4.e7006_e5984_s3126_r10201_r10210_p3956.root',

    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.DAOD_EGAM3.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.DAOD_EGAM3.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.DAOD_EGAM3.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.DAOD_EGAM3.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.DAOD_EGAM3.e7006_e5984_s3126_r10724_r10726_p3956.root',
    
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.DAOD_EGAM4.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.DAOD_EGAM4.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.DAOD_EGAM4.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.DAOD_EGAM4.e7006_e5984_s3126_r10724_r10726_p3956.root',
    'mc16e_13TeV/00-03-01/mc16e.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.DAOD_EGAM4.e7006_e5984_s3126_r10724_r10726_p3956.root',
]

for f in files:

    print(f)

    tree = ROOT.TChain('output')
    tree.Add(files_loc_ZLLG + f)

    out_str = ''

    data_type, version, fname = f.split('/')


    is_data = False
    if data_type.startswith('data'):
        is_data = True
        type_ = 'Zeey' if 'Zeeg' in fname else 'Zmumuy'
        out_name = 'events_%s_%s.txt' % (type_, data_type)
    else:
        _, name, deriv, tags, _ = fname.split('.')
        dsid = dsids[name]
        type_ = 'Zeey' if 'eegamma' in name  else 'Zmumuy'
        out_name = 'events_%s_%s_%s.txt' % (type_, data_type, dsid)

    for event in tree:

        ph_abseta = abs(getattr(event, 'ph.eta2'))
        ph_pt = getattr(event, 'ph.pt') * 0.001

        # acceptance selection
        if ph_abseta > 2.37 or (ph_abseta > 1.37 and ph_abseta < 1.52):
            continue

        if ph_pt < 10:
            continue

        ph_eta = getattr(event, 'ph.eta2')
        ph_phi = getattr(event, 'ph.phi')

        if is_data:
            run_number = getattr(event, 'EventInfo.runNumber')
        else:
            run_number = int(dsid)

            # # if run number is different save anc changed file
            # if previous_run_number > 0 and run_number != previous_run_number:
            #     with open(out_name.replace('XXXXXX', str( previous_run_number)), 'w+') as of:
            #         of.write(out_str)
          
            #     out_str = ''
            
            # previous_run_number = run_number


        # 
        event_number = getattr(event, 'EventInfo.eventNumber')

        #out_str += '%i   %i   %.3f   %.3f   %.3f\n' % (run_number, event_number, ph_eta, ph_phi, ph_pt)
        out_str += '%i   %i   %.4f   %.4f   \n' % (run_number, event_number, ph_eta, ph_phi)


    # only or last run
    with open(out_name, 'w+') as of:
        of.write(out_str)
