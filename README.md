# PhotonCellsNtuple
                               
# Setup 

    source setup.sh

# Compile

    mkdir build
    cd buid
    cmake ../source
    make
    source x86_64-centos7-gcc62-opt/setup.sh


# Prepare input file with events

The first step is to create the files with the event number and photon information for the events passing a minimal selection from the official ntuples

    python get_events_photon_Zrad.py

or

    root -q get_events_photon_SP.cxx



# Local test 

    athena -c "events_file=eventsXXX.txt; isMC=True;" LocalRun_jobOptions.py



# To run on the grid:

    pathena -c "events_file=eventsXXX.txt; isMC=True;" GridRun_jobOptions.py --inDS={input}  --outDS={output} --nGBPerJob=MAX --extFile=eventsXXX.txt


where eventsXXX.txt is the file with the events generated in the previous step for the corresponding input sample. You need to change also the isMC flag.

You can use the submit.py script in the run directory as an example.

# Status of Jan Lukas and Björn:

We managed to perform every step successfully, except for 'To run on the grid:'.
In this fork you can find an updated version of 'run/submit.py', where we added data AODs because cell-level information is not available in SP DAODs or they are not available anyway. Every file except one is commented out for testing purposes.
Please note that the input .txt files generated during the step 'Prepare input file with events' are in run/input_files. All of these files except one example file are ignored by git.
We assume that

    python submit.py

in run/ would be sufficient to send the jobs to the grid (after lsetup panda and so on). We have a technical problem, even after commenting out lines 3 and 6 in 'GridRun_jobOptions.py', where a test_file was added. It appears that 'inputFilePeeker.py' is called with an invalid inFile. Please find a logfile for this in the run directory.
