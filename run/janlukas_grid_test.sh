setupATLAS
asetup 21.2.109.0,AthDerivation
lsetup rucio
voms-proxy-init -voms atlas
lsetup panda

# The output of this can be found in log_error_submit.py.log
python submit.py

# The output of this can be found in log_error_grid_test.log
#pathena -c "events_file='input_files/events_SP_gammajet_mc16a_423099.txt' ; isMC=True" GridRun_jobOptions.py --inDS=mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.recon.AOD.e4453_s3126_r9364 --outDS=user.jspaeh.mc16a_423099_SP_gammajet_cells_v5 --nGBPerJob=MAX --extFile="input_files/events_SP_gammajet_mc16a_423099.txt"
