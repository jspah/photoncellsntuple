import os
import glob

username = 'jspah'
version = 'v1'

samples = [
    # SP AOD (because no cell level info in SP DAOD or SP DAOD missing)
    'data15_13TeV.periodAllYear.physics_Main.PhysCont.AOD.repro21_v02',
    'data16_13TeV.periodAllYear.physics_Main.PhysCont.AOD.repro21_v01',
    'data17_13TeV.periodAllYear.physics_Main.PhysCont.AOD.pro22_v03',
    'data18_13TeV.periodC.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodE.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodD.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodB.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodF.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodG4.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodG1.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodG3.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodJ.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodG2.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodG5.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodH.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodI.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodK.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodM.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodL.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodO.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodQ.physics_Main.PhysCont.AOD.t0pro22_v01',
    'data18_13TeV.periodN.physics_Main.PhysCont.AOD.t0pro22_v01',

    #'data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp15_v01_p3948',
    # 'data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp16_v01_p3948',
    # 'data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp17_v01_p3948',
    # 'data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp18_v01_p3948',

    # 'data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM4.grp15_v01_p3948',
    # 'data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM4.grp16_v01_p3948',
    # 'data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM4.grp17_v01_p3948',
    # 'data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM4.grp18_v01_p3948',

    # 'mc16_13TeV.366140.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366141.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366142.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366143.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366144.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956',

    # 'mc16_13TeV.366140.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.deriv.DAOD_EGAM3.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366141.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.deriv.DAOD_EGAM3.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366142.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.deriv.DAOD_EGAM3.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366143.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.deriv.DAOD_EGAM3.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366144.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM3.e7006_s3126_r10201_p3956',

    # 'mc16_13TeV.366140.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.deriv.DAOD_EGAM3.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366141.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.deriv.DAOD_EGAM3.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366142.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.deriv.DAOD_EGAM3.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366143.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.deriv.DAOD_EGAM3.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366144.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM3.e7006_s3126_r10724_p3956',

    #'mc16_13TeV.366145.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.deriv.DAOD_EGAM4.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366146.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.deriv.DAOD_EGAM4.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366147.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.deriv.DAOD_EGAM4.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366148.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.deriv.DAOD_EGAM4.e7006_s3126_r9364_p3956',
    # 'mc16_13TeV.366149.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM4.e7006_s3126_r9364_p3956',

    # 'mc16_13TeV.366145.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.deriv.DAOD_EGAM4.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366146.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.deriv.DAOD_EGAM4.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366147.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.deriv.DAOD_EGAM4.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366148.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.deriv.DAOD_EGAM4.e7006_s3126_r10201_p3956',
    # 'mc16_13TeV.366149.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM4.e7006_s3126_r10201_p3956',

    # 'mc16_13TeV.366145.Sh_224_NN30NNLO_mumugamma_LO_pty_7_15.deriv.DAOD_EGAM4.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366146.Sh_224_NN30NNLO_mumugamma_LO_pty_15_35.deriv.DAOD_EGAM4.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366147.Sh_224_NN30NNLO_mumugamma_LO_pty_35_70.deriv.DAOD_EGAM4.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366148.Sh_224_NN30NNLO_mumugamma_LO_pty_70_140.deriv.DAOD_EGAM4.e7006_s3126_r10724_p3956',
    # 'mc16_13TeV.366149.Sh_224_NN30NNLO_mumugamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM4.e7006_s3126_r10724_p3956',

    # SP MC AOD
    'mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423100.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP17_35.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423101.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP35_50.recon.AOD.e3904_s3126_r9364',
    'mc16_13TeV.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423104.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP140_280.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423105.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP280_500.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423106.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP500_800.recon.AOD.e3791_s3126_r9364',
    'mc16_13TeV.423107.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP800_1000.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423108.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1000_1500.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423109.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1500_2000.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423110.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2000_2500.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423111.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2500_3000.recon.AOD.e4453_s3126_r9364',
    'mc16_13TeV.423112.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP3000_inf.recon.AOD.e4453_s3126_r9364',

    'mc16_13TeV.423303.Pythia8EvtGen_A14NNPDF23LO_perf_JF50.recon.AOD.e3848_s3126_r9364',
    'mc16_13TeV.423302.Pythia8EvtGen_A14NNPDF23LO_perf_JF35.recon.AOD.e3848_s3126_r9364',
    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.recon.AOD.e3848_s3126_r9364',

    'mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423100.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP17_35.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423101.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP35_50.recon.AOD.e3904_s3126_r10201',
    'mc16_13TeV.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423104.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP140_280.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423105.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP280_500.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423106.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP500_800.recon.AOD.e3791_s3126_r10201',
    'mc16_13TeV.423107.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP800_1000.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423108.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1000_1500.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423109.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP1500_2000.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423110.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2000_2500.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423111.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP2500_3000.recon.AOD.e4453_s3126_r10201',
    'mc16_13TeV.423112.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP3000_inf.recon.AOD.e4453_s3126_r10201',

    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.recon.AOD.e3848_s3126_r10201',
    'mc16_13TeV.423301.Pythia8EvtGen_A14NNPDF23LO_perf_JF23.recon.AOD.e3848_s3126_r10201',
    'mc16_13TeV.423302.Pythia8EvtGen_A14NNPDF23LO_perf_JF35.recon.AOD.e3848_s3126_r10201',
    'mc16_13TeV.423303.Pythia8EvtGen_A14NNPDF23LO_perf_JF50.recon.AOD.e3848_s3126_r10201',

]

for s in samples:

    opts = dict()

    opts['inputs'] = s

    datatype, dsid, name, _, deriv, tags = s.split('.')

    if datatype.startswith('mc'):
        if 'r9364' in tags:
            datatype = 'mc16a'
        elif 'r10201' in tags:
            datatype = 'mc16d'
        else:
            datatype = 'mc16e'
    else:
        datatype = datatype.replace('_13TeV', '')

    if deriv == 'DAOD_EGAM3':
        events_type = 'Zeey'
    elif deriv == 'DAOD_EGAM4':
        events_type = 'Zmumuy'
    elif deriv == 'AOD' and 'data' in datatype:
        events_type = 'SP'
    elif deriv == 'AOD' and 'gammajet' in name:
        events_type = 'SP_gammajet'
    elif deriv == 'AOD' and 'perf_JF' in name:
        events_type = 'SP_jetjet'

    opts['outputs'] = 'user.%s.%s_%s_%s_cells_%s' % (username, datatype, dsid, events_type, version)

    if datatype.startswith('mc'):
        opts['is_MC'] = 'True'
        opts['events_file'] = 'input_files/events_%s_%s_%s.txt' % (events_type, datatype, dsid)
    else:
        opts['is_MC'] = 'False'
        opts['events_file'] = 'input_files/events_%s_%s.txt' % (events_type, datatype)

    cmd = 'pathena -c "events_file=\'{events_file}\' ; isMC={is_MC}" GridRun_jobOptions.py --inDS={inputs} --outDS={outputs} --nGBPerJob=MAX --extFile="{events_file}"'.format(**opts)

    print(cmd)
    os.system(cmd)
