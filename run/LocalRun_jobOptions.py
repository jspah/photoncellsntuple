#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here


#test_file = '/eos/user/f/falonso/data/PhotonID/test/AOD.11189772._000111.pool.root.1'
##test_file = '/eos/user/f/falonso/data/PhotonID/test/DAOD_EGAM3.19062552._000085.pool.root.1'
#test_file = '/eos/user/f/falonso/data/PhotonID/Cells/Zrad_00-03-01/mc16_13TeV/DAOD_EGAM3.19062512._000059.pool.root.1'
test_file = '/ceph/groups/e4/users/jspaeh/private/tmp/mc16_13TeV.366140.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.deriv.DAOD_EGAM3.e7006_s3126_r9364_p3956/DAOD_EGAM3.19062512._000047.pool.root.1'

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [test_file] 

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "POOLAccess" 

from RecExConfig import AutoConfiguration
AutoConfiguration.ConfigureSimulationOrRealData() #configures DataSource global flag
AutoConfiguration.ConfigureConditionsTag() #sets globalflags.ConditionsTag
from AthenaCommon.DetFlags import DetFlags
DetFlags.all_setOff() #skip this line out to leave everything on. But this will take longer
DetFlags.detdescr.Calo_setOn() #e.g. if I am accessing CaloCellContainer, I need the calo detector description
include("RecExCond/AllDet_detDescr.py")

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ('MyxAODAnalysis', 'AnalysisAlg')
alg.EventsFile = events_file
alg.IsMC = isMC 

# later on we'll add some configuration options for our algorithm that go here

from egammaRec.Factories import ToolFactory
from CaloClusterCorrection.CaloClusterCorrectionConf import CaloFillRectangularCluster as CFRC
CaloFillRectangularCluster = ToolFactory(CFRC, 
                                         cells_name = "AODCellContainer", 
                                         fill_cluster = True)

ToolSvc = [CaloFillRectangularCluster.copy(name = "CaloFillRectangularCluster_7x11", eta_size = 7, phi_size = 11)()]


jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs.root"]

# Add our algorithm to the main alg sequence
athAlgSeq += alg


# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

