#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include "PathResolver/PathResolver.h"

#include <fstream>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("EventsFile", m_events_file);
  declareProperty("IsMC", m_isMC);
}

StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
	
  SetupTree();

  m_previous_run_number = 0;

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  ClearVariables();
  
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // Event number selection
  run_number = eventInfo->runNumber();
  event_number = eventInfo->eventNumber();

  if (run_number != m_previous_run_number) {
    readEventsFile(run_number);
    m_previous_run_number = run_number;
  }
  
  if ( !std::count(m_selectedEvents.begin(), m_selectedEvents.end(), event_number) )
    return StatusCode::SUCCESS; 

  ANA_MSG_INFO("Processing event " << event_number);

  // Selected photon to match
  MatchPhoton sel_photon = m_selectedPhotons[event_number];

  // Photon selection
  const xAOD::PhotonContainer *photons(0);
  ANA_CHECK(evtStore()->retrieve(photons, "Photons"));

  m_photons.clear();

  double pt, eta, phi, dR2, matched_dR2 = 999;
  unsigned int matched_idx = -1;
  for (const auto& ph : *photons) {  

    pt  = ph->pt() * 0.001;
    eta = ( ph->caloCluster() ) ? ph->caloCluster()->etaBE(2) : -999.0;
    phi = ph->phi();

    if (pt  < 5.) 
      continue;
    if (fabs(eta) > 2.4) 
      continue;

    // Try to match photon with the selected one in the official framework using dR
    dR2 = (eta - sel_photon.eta) * (eta - sel_photon.eta) + (phi - sel_photon.phi) * (phi - sel_photon.phi);

    if (dR2  > 0.1)
      continue;

    m_photons.push_back(ph);

    if (dR2 < matched_dR2) {
      matched_dR2 = dR2;
      matched_idx = m_photons.size() - 1;
    }

  }

  if (m_photons.size() == 0) {
    ANA_MSG_WARNING("Match failed! No photon matched with (eta, phi) = (" << sel_photon.eta << "," << sel_photon.phi << "). Skipping event ... ");
    return StatusCode::SUCCESS;
  }

  if (m_photons.size() > 1) {
    ANA_MSG_INFO("Match not unique. " << m_photons.size() << " photons matched. Selecting closest photon ... ");
  }

  const xAOD::Photon *photon = m_photons[matched_idx];

  ph_pt  = photon->pt() * 0.001;
  ph_eta = ( photon->caloCluster() ) ? photon->caloCluster()->etaBE(2) : -999.0;
  ph_phi = photon->phi();

  ANA_MSG_DEBUG("Matched photon: (pt, eta, phi) = " << Form("(%.2f, %.2f, %.2f)", ph_pt, ph_eta, ph_phi) << " - Original photon: (pt, eta, phi) = " << Form("(%.2f, %.2f)", sel_photon.eta, sel_photon.phi));
  
  fillClusterInfo(photon, 7, 11);
  
  tree("ntuple_cells")->Fill();

  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

void MyxAODAnalysis :: ClearVariables()
{
  run_number = 0;
  event_number = 0;
  
  ph_pt             = 0.;
  ph_eta            = 0.;
  ph_phi            = 0.;

  ph_ClusterSize_L0 = 0;
  ph_ClusterSize_L1 = 0;
  ph_ClusterSize_L2 = 0;
  ph_ClusterSize_L3 = 0;
  
  ph_ClusterCells_L0_E.clear();
  ph_ClusterCells_L0_Eta.clear();
  ph_ClusterCells_L0_Phi.clear();
  ph_ClusterCells_L0_X.clear();
  ph_ClusterCells_L0_Y.clear();
  ph_ClusterCells_L0_Z.clear();
  ph_ClusterCells_L0_T.clear();
  ph_ClusterCells_L0_isBAD.clear();
  
  ph_ClusterCells_L1_E.clear();
  ph_ClusterCells_L1_Eta.clear();
  ph_ClusterCells_L1_Phi.clear();
  ph_ClusterCells_L1_X.clear();
  ph_ClusterCells_L1_Y.clear();
  ph_ClusterCells_L1_Z.clear();
  ph_ClusterCells_L1_T.clear();
  ph_ClusterCells_L1_isBAD.clear();
  
  ph_ClusterCells_L2_E.clear();
  ph_ClusterCells_L2_Eta.clear();
  ph_ClusterCells_L2_Phi.clear();
  ph_ClusterCells_L2_X.clear();
  ph_ClusterCells_L2_Y.clear();
  ph_ClusterCells_L2_Z.clear();
  ph_ClusterCells_L2_T.clear();
  ph_ClusterCells_L2_isBAD.clear();
  
  ph_ClusterCells_L3_E.clear();
  ph_ClusterCells_L3_Eta.clear();
  ph_ClusterCells_L3_Phi.clear();
  ph_ClusterCells_L3_X.clear();
  ph_ClusterCells_L3_Y.clear();
  ph_ClusterCells_L3_Z.clear();
  ph_ClusterCells_L3_T.clear();
  ph_ClusterCells_L3_isBAD.clear();
  
}


void MyxAODAnalysis :: SetupTree()
{
  StatusCode st = book(TTree("ntuple_cells", "ntuple_cells"));
  if ( st != StatusCode::SUCCESS ) { 
    ANA_MSG_ERROR("Failed to setup output Tree");
    return;
  }

  tree ("ntuple_cells")->Branch("RunNumber", &run_number);
  tree ("ntuple_cells")->Branch("EventNumber", &event_number);
  // tree ("ntuple_cells")->Branch("DSID", &mc_dsid, "DSID/I");
  
  tree ("ntuple_cells")->Branch("ph_pt", &ph_pt);
  tree ("ntuple_cells")->Branch("ph_eta", &ph_eta);
  tree ("ntuple_cells")->Branch("ph_phi", &ph_phi);

  // std::string windows[3] = {"3x7", "5x5", "7x11"};

  tree ("ntuple_cells")->Branch("ph_ClusterSize_7x11_L0",  &ph_ClusterSize_L0);
  tree ("ntuple_cells")->Branch("ph_ClusterSize_7x11_L1",  &ph_ClusterSize_L1);
  tree ("ntuple_cells")->Branch("ph_ClusterSize_7x11_L2",  &ph_ClusterSize_L2);
  tree ("ntuple_cells")->Branch("ph_ClusterSize_7x11_L3",  &ph_ClusterSize_L3);
  
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_E",     &ph_ClusterCells_L0_E);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_Eta",   &ph_ClusterCells_L0_Eta);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_Phi",   &ph_ClusterCells_L0_Phi);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_X",     &ph_ClusterCells_L0_X);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_Y",     &ph_ClusterCells_L0_Y);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_Z",     &ph_ClusterCells_L0_Z);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_T",     &ph_ClusterCells_L0_T);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L0_isBAD", &ph_ClusterCells_L0_isBAD);
  
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_E",     &ph_ClusterCells_L1_E);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_Eta",   &ph_ClusterCells_L1_Eta);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_Phi",   &ph_ClusterCells_L1_Phi);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_X",     &ph_ClusterCells_L1_X);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_Y",     &ph_ClusterCells_L1_Y);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_Z",     &ph_ClusterCells_L1_Z);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_T",     &ph_ClusterCells_L1_T);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L1_isBAD", &ph_ClusterCells_L1_isBAD);
  
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_E",     &ph_ClusterCells_L2_E);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_Eta",   &ph_ClusterCells_L2_Eta);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_Phi",   &ph_ClusterCells_L2_Phi);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_X",     &ph_ClusterCells_L2_X);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_Y",     &ph_ClusterCells_L2_Y);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_Z",     &ph_ClusterCells_L2_Z);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_T",     &ph_ClusterCells_L2_T);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L2_isBAD", &ph_ClusterCells_L2_isBAD);
  
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_E",     &ph_ClusterCells_L3_E);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_Eta",   &ph_ClusterCells_L3_Eta);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_Phi",   &ph_ClusterCells_L3_Phi);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_X",     &ph_ClusterCells_L3_X);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_Y",     &ph_ClusterCells_L3_Y);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_Z",     &ph_ClusterCells_L3_Z);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_T",     &ph_ClusterCells_L3_T);
  tree ("ntuple_cells")->Branch("ph_ClusterCells_7x11_L3_isBAD", &ph_ClusterCells_L3_isBAD);
  
}


void MyxAODAnalysis :: fillClusterInfo(const xAOD::Photon*& egamma, int m_eta_size, int m_phi_size)
{

  if (not egamma or not egamma->caloCluster()) return;

  const CaloCellContainer* cellCont(0);
  xAOD::CaloCluster* egcClone(0);

  bool isBarrel = xAOD::EgammaHelpers::isBarrel(egamma);
  CaloCell_ID::CaloSample sample = isBarrel ? CaloCell_ID::EMB2 : CaloCell_ID::EME2;

  double etaCalo = 0.;
  if(! (egamma->caloCluster()->retrieveMoment(xAOD::CaloCluster::ETACALOFRAME, etaCalo))) {
    ANA_MSG_ERROR("Failed to retrieve eta from calo");
  }
  double phiCalo = 0.;
  if(! (egamma->caloCluster()->retrieveMoment(xAOD::CaloCluster::PHICALOFRAME, phiCalo))) {
    ANA_MSG_ERROR("Failed to retrieve phi from calo");
  }

  //double eta0 = egamma->caloCluster()->eta0() + etaCalo - egamma->caloCluster()->eta();
  //double phi0 = egamma->caloCluster()->phi0() + phiCalo - egamma->caloCluster()->phi();

  CaloCellDetPos  *ccdp; 
  double etamaxClus, phimaxClus, etamaxClusCalo, phimaxClusCalo;
  if (isBarrel) {
    etamaxClus = egamma->caloCluster()->etamax(CaloCell_ID::EMB2);
    phimaxClus = egamma->caloCluster()->phimax(CaloCell_ID::EMB2);
  }
  else {
    etamaxClus = egamma->caloCluster()->etamax(CaloCell_ID::EME2);
    phimaxClus = egamma->caloCluster()->phimax(CaloCell_ID::EME2);
  }
  
   ccdp->getDetPosition(sample, etamaxClus, phimaxClus, etamaxClusCalo, phimaxClusCalo); 
   caloFillRectangularTools.setTypeAndName(Form("CaloFillRectangularCluster/CaloFillRectangularCluster_%dx%d", m_eta_size, m_phi_size));
   StatusCode isDone = caloFillRectangularTools.retrieve();
   if( isDone != StatusCode::SUCCESS) return;
   Calotool = dynamic_cast<CaloFillRectangularCluster* >( &(*caloFillRectangularTools ));

   if (evtStore()->retrieve(cellCont,"AODCellContainer").isFailure())
     ANA_MSG_WARNING("AODCellContainer" << " not found");
   else if (not egamma->author(xAOD::EgammaParameters::AuthorCaloTopo35) ) {  
     egcClone = CaloClusterStoreHelper::makeCluster( cellCont,
                                                     etamaxClusCalo,
                                                     phimaxClusCalo,
                                                     egamma->caloCluster()->clusterSize());
     Calotool->makeCorrection(egcClone);
   }
   
   auto first_cell = egcClone->cell_begin();
   auto last_cell = egcClone->cell_end();
   int ncelll1 = 0 ;
   int ncelll2 = 0 ;
   int ncelll3 = 0;
   int npres = 0;
   int ncell = 0;   
   std::vector<float> EClusterLr1Eta, EClusterLr1Phi, EClusterLr1E, EClusterLr1X, EClusterLr1Y, EClusterLr1Z, EClusterLr1T; std::vector<bool> EClusterLr1isBAD;
   std::vector<float> EClusterLr2Eta, EClusterLr2Phi, EClusterLr2E, EClusterLr2X, EClusterLr2Y, EClusterLr2Z, EClusterLr2T; std::vector<bool> EClusterLr2isBAD;
   std::vector<float> EClusterLr3Eta, EClusterLr3Phi, EClusterLr3E, EClusterLr3X, EClusterLr3Y, EClusterLr3Z, EClusterLr3T; std::vector<bool> EClusterLr3isBAD;
   std::vector<float> EClusterPreSEta, EClusterPreSPhi, EClusterPreSE, EClusterPreSX, EClusterPreSY, EClusterPreSZ, EClusterPreST; std::vector<bool> EClusterPreSisBAD;
   
   for (;first_cell != last_cell; ++first_cell,++ncell) {
     const CaloCell* tcell = *first_cell;
     int sampling = tcell->caloDDE()->getSampling();
     
     if( sampling== CaloCell_ID::EMB1 || sampling== CaloCell_ID::EME1 ){
       EClusterLr1Eta.push_back(tcell->eta());
       EClusterLr1Phi.push_back(tcell->phi());
       EClusterLr1E.push_back(tcell->e());
       
       EClusterLr1X.push_back(tcell->x());
       EClusterLr1Y.push_back(tcell->y());
       EClusterLr1Z.push_back(tcell->z());
       EClusterLr1T.push_back(tcell->time());
       EClusterLr1isBAD.push_back(tcell->badcell());     	
       
       ncelll1++;
     }
     
     if(sampling== CaloCell_ID::EMB2 || sampling== CaloCell_ID::EME2 ){
       EClusterLr2Eta.push_back(tcell->eta());
       EClusterLr2Phi.push_back(tcell->phi());
       EClusterLr2E.push_back(tcell->e());
       
       EClusterLr2X.push_back(tcell->x());
       EClusterLr2Y.push_back(tcell->y());
       EClusterLr2Z.push_back(tcell->z());
       EClusterLr2T.push_back(tcell->time());
       EClusterLr2isBAD.push_back(tcell->badcell());
       
       ncelll2++;
     }
     
     if(sampling== CaloCell_ID::EMB3 || sampling== CaloCell_ID::EME3 ){
       EClusterLr3Eta.push_back(tcell->eta());
       EClusterLr3Phi.push_back(tcell->phi());
       EClusterLr3E.push_back(tcell->e());
       
       EClusterLr3X.push_back(tcell->x());
       EClusterLr3Y.push_back(tcell->y());
       EClusterLr3Z.push_back(tcell->z());
       EClusterLr3T.push_back(tcell->time());
       EClusterLr3isBAD.push_back(tcell->badcell());
       
      ncelll3++;
     }
     
     if( sampling== CaloCell_ID::PreSamplerB || sampling== CaloCell_ID::PreSamplerE ){
       EClusterPreSEta.push_back(tcell->eta());
       EClusterPreSPhi.push_back(tcell->phi());
       EClusterPreSE.push_back(tcell->e());
       
       EClusterPreSX.push_back(tcell->x());
       EClusterPreSY.push_back(tcell->y());
       EClusterPreSZ.push_back(tcell->z());
       EClusterPreST.push_back(tcell->time());
       EClusterPreSisBAD.push_back(tcell->badcell());
       
       npres++;
     }

   }

   ph_ClusterCells_L0_Eta   = EClusterPreSEta;
   ph_ClusterCells_L0_Phi   = EClusterPreSPhi;
   ph_ClusterCells_L0_E     = EClusterPreSE;
   ph_ClusterCells_L0_X     = EClusterPreSX;
   ph_ClusterCells_L0_Y     = EClusterPreSY;
   ph_ClusterCells_L0_Z     = EClusterPreSZ;
   ph_ClusterCells_L0_T     = EClusterPreST;
   ph_ClusterCells_L0_isBAD = EClusterPreSisBAD;

   ph_ClusterCells_L1_Eta   = EClusterLr1Eta;
   ph_ClusterCells_L1_Phi   = EClusterLr1Phi;
   ph_ClusterCells_L1_E     = EClusterLr1E;
   ph_ClusterCells_L1_X     = EClusterLr1X;
   ph_ClusterCells_L1_Y     = EClusterLr1Y;
   ph_ClusterCells_L1_Z     = EClusterLr1Z;
   ph_ClusterCells_L1_T     = EClusterLr1T;
   ph_ClusterCells_L1_isBAD = EClusterLr1isBAD;

   ph_ClusterCells_L2_Eta   = EClusterLr2Eta;
   ph_ClusterCells_L2_Phi   = EClusterLr2Phi;
   ph_ClusterCells_L2_E     = EClusterLr2E;
   ph_ClusterCells_L2_X     = EClusterLr2X;
   ph_ClusterCells_L2_Y     = EClusterLr2Y;
   ph_ClusterCells_L2_Z     = EClusterLr2Z;
   ph_ClusterCells_L2_T     = EClusterLr2T;
   ph_ClusterCells_L2_isBAD = EClusterLr2isBAD;

   ph_ClusterCells_L3_Eta   = EClusterLr3Eta;
   ph_ClusterCells_L3_Phi   = EClusterLr3Phi;
   ph_ClusterCells_L3_E     = EClusterLr3E;
   ph_ClusterCells_L3_X     = EClusterLr3X;
   ph_ClusterCells_L3_Y     = EClusterLr3Y;
   ph_ClusterCells_L3_Z     = EClusterLr3Z;
   ph_ClusterCells_L3_T     = EClusterLr3T;
   ph_ClusterCells_L3_isBAD = EClusterLr3isBAD;
      
   ph_ClusterSize_L0 = npres;
   ph_ClusterSize_L1 = ncelll1;
   ph_ClusterSize_L2 = ncelll2;
   ph_ClusterSize_L3 = ncelll3;
   
   delete egcClone;
   
}

void MyxAODAnalysis::readEventsFile(unsigned int run_number)
{
  m_selectedEvents.clear();
  m_selectedPhotons.clear();

  //TString events_file = Form("MyAnalysis/%s", m_events_file.c_str());
  TString events_file = Form("%s", m_events_file.c_str());
  // if (m_events_file.rfind("data", 0) == 0) {
  //   events_file = events_file.ReplaceAll("XXXXXX", Form("%u", run_number));
  // }

  if (m_isMC)
    ANA_MSG_INFO("Reading events from file " << events_file.Data());
  else
    ANA_MSG_INFO("Reading events from file " << events_file.Data() << " with RunNumber = " << run_number);

  // std::ifstream infile(PathResolverFindCalibFile(events_file.Data()));
  std::ifstream infile(events_file.Data());

  unsigned int run;
  unsigned long long event;
  float eta, phi;
  while (infile >> run >> event >> eta >> phi) {

    if (!m_isMC && run != run_number)
      continue;

    m_selectedEvents.push_back(event);

    MatchPhoton p;
    p.eta = eta;
    p.phi = phi;

    m_selectedPhotons[event] = p;
  }

  ANA_MSG_INFO("Loaded " << m_selectedEvents.size() << " events from file");
}
