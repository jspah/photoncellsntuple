#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODEventInfo/EventInfo.h>

//Tools include(s) :
#include <AsgTools/AnaToolHandle.h>
#include "GaudiKernel/ToolHandle.h"
#include "PathResolver/PathResolver.h"

//ROOTCORE include(s) :
#include "xAODCore/ShallowCopy.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"

#include "xAODEgamma/EgammaAuxContainer.h"
#include "xAODEgamma/Egamma.h"

#include "TruthUtils/PIDHelpers.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticle.h"


//Athena include(s) :
#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloUtils/CaloClusterStoreHelper.h"
#include "CaloUtils/CaloCellDetPos.h"
#include "CaloClusterCorrection/CaloFillRectangularCluster.h"
#include "GaudiKernel/ToolHandle.h"

struct MatchPhoton {
  double eta;
  double phi;
};


class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  void   readEventsFile(unsigned int);
  void   fillClusterInfo(const xAOD::Photon*& egamma, int m_eta_size, int m_phi_size);
  void   ClearVariables();
  void   SetupTree();

  ToolHandle<CaloClusterCollectionProcessor> caloFillRectangularTools;
  CaloFillRectangularCluster *Calotool;

  std::vector< const xAOD::Photon* > m_photons;
  const xAOD::EventInfo *eventInfo = nullptr;

  unsigned int m_previous_run_number;
  std::string m_events_file;
  bool m_isMC;

  /// List of selected events
  std::vector<unsigned long long> m_selectedEvents;
  std::map<unsigned long long, MatchPhoton> m_selectedPhotons;

  unsigned int run_number;
  unsigned long long event_number;
  // Int_t       mc_dsid;

  double ph_pt;
  double ph_eta;
  double ph_phi;

  int ph_ClusterSize_L0;
  int ph_ClusterSize_L1;
  int ph_ClusterSize_L2;
  int ph_ClusterSize_L3;

  std::vector<float> ph_ClusterCells_L0_E;
  std::vector<float> ph_ClusterCells_L0_Eta;
  std::vector<float> ph_ClusterCells_L0_Phi;
  std::vector<float> ph_ClusterCells_L0_X;
  std::vector<float> ph_ClusterCells_L0_Y;
  std::vector<float> ph_ClusterCells_L0_Z;
  std::vector<float> ph_ClusterCells_L0_T;
  std::vector<bool>  ph_ClusterCells_L0_isBAD;

  std::vector<float> ph_ClusterCells_L1_E;
  std::vector<float> ph_ClusterCells_L1_Eta;
  std::vector<float> ph_ClusterCells_L1_Phi;
  std::vector<float> ph_ClusterCells_L1_X;
  std::vector<float> ph_ClusterCells_L1_Y;
  std::vector<float> ph_ClusterCells_L1_Z;
  std::vector<float> ph_ClusterCells_L1_T;
  std::vector<bool>  ph_ClusterCells_L1_isBAD;

  std::vector<float> ph_ClusterCells_L2_E;
  std::vector<float> ph_ClusterCells_L2_Eta;
  std::vector<float> ph_ClusterCells_L2_Phi;
  std::vector<float> ph_ClusterCells_L2_X;
  std::vector<float> ph_ClusterCells_L2_Y;
  std::vector<float> ph_ClusterCells_L2_Z;
  std::vector<float> ph_ClusterCells_L2_T;
  std::vector<bool>  ph_ClusterCells_L2_isBAD;

  std::vector<float> ph_ClusterCells_L3_E;
  std::vector<float> ph_ClusterCells_L3_Eta;
  std::vector<float> ph_ClusterCells_L3_Phi;
  std::vector<float> ph_ClusterCells_L3_X;
  std::vector<float> ph_ClusterCells_L3_Y;
  std::vector<float> ph_ClusterCells_L3_Z;
  std::vector<float> ph_ClusterCells_L3_T;
  std::vector<bool>  ph_ClusterCells_L3_isBAD;

};

#endif
